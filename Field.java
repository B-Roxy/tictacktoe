import java.util.Arrays;

/**
 * Created by Nasty on 19.04.2017.
 */
public class Field {
    protected final static int size = 3;
    protected final static int EMPTY = -1;
    protected final static int ZERO = 0;
    protected final static int X = 1;
    private int[][] cell = new int[size][size];

    public boolean isEqualValue(int i, int j, int iVal){
        return cell[i][j] == iVal;
    }

    public Field(){
        FillField();
    }

    private void FillField() {
        for (int[] row : cell) {
            Arrays.fill(row, EMPTY);
        }
    }

    protected void SetValue(int i, int j, int val){
        if (cell[i][j] == EMPTY) {
            cell[i][j] = val;
        }
        else {
            System.out.format("The cell (%d,%d) is BUSY%n", i+1, j+1);
        }
    }


    public void PrintField(){
        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++) {
                if (cell[i][j] == EMPTY) {
                    System.out.print("-");
                }
                else {
                    System.out.print(cell[i][j] == ZERO ? "0" : "X");
                }
            }
            System.out.println();
        }
        System.out.println();
    }
}
