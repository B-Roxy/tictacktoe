import java.awt.*;

/**
 * Created by Nasty on 19.04.2017.
 */
public class Computer extends Gamer {
//    protected String name = "Computer";
    public void DoStep(Point optimalPoint, Field field) {
        Point target;
    // = Search(field);
        if (optimalPoint == null) {
            target = SearchPriority(field);
        }
        else {
            target = optimalPoint;
        }
        field.SetValue(target.x, target.y, weapon);
    }

    private Point SearchPriority(Field field) {
        Point[] priorityPoint = {new Point(1, 1),
                new Point(0, 0), new Point(2, 2), new Point(0, 2), new Point(2, 0),
                new Point(0, 1), new Point(1, 0), new Point(2, 1), new Point(1, 2)};
        for (Point elem : priorityPoint) {
            if (field.isEqualValue(elem.x, elem.y, Field.EMPTY)) {
                return elem;
            }
        }
        return null;
    }
}
