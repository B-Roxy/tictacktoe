import java.awt.*;
import java.util.Scanner;

/**
 * Created by Nasty on 19.04.2017.
 */
public class Game {
    private Field field = new Field();
    private Gamer gamer1;
    private Gamer gamer2;
    private boolean isQueue; // value == true ? (the gamer1 steps) : (the gamer2 steps)
    // result has following value
    // -1: game continues
    //  0: dead heat
    //  1: the gamer X wins
    //  2: the gamer 0 wins
    protected static int result = -1;
    private Scanner in = new Scanner(System.in);

    protected void GameStart(){
        // System.out.print("Would you like to play with other person(Y/N)? ");
        gamer1 = new User();
        gamer2 = new Computer();

        if (Math.random() > 0.5){
            isQueue = true;
            gamer1.setWeapon(Field.X);
            gamer2.setWeapon(Field.ZERO);
            System.out.println("Congratulations! Your weapon is 'X'");
        }
        else {
            isQueue = false;
            gamer1.setWeapon(Field.ZERO);
            gamer2.setWeapon(Field.X);
            System.out.println("Your weapon is '0'");
        }
        Point optimalPoint = null;
        while (result == -1){
            if (isQueue) {
                gamer1.DoStep(optimalPoint, field);
            } else {
                gamer2.DoStep(optimalPoint, field);
            }
            optimalPoint = Search();
            field.PrintField();
            isQueue = !isQueue;
        }
        switch (result) {
            case 0:
                System.out.println("Nobody wins at this time");
                break;
            case 1:
                System.out.println("Gamer with weapon 'X' is winner!");
                break;
            case 2:
                System.out.println("Gamer with weapon '0' is winner!");
                break;
        }
    }

    private Point Search() {
        int count0;
        int countX;
        int countFree;
        int totalCountFree = 0;
        Point freePoint;
        // row
        for (int i = 0; i < Field.size; i++) {
            freePoint = new Point(i, 0);
            count0 = 0;
            countX = 0;
            countFree = 0;
            for (int j = 0; j < Field.size; j++) {
                if (field.isEqualValue(i, j, Field.ZERO)) {
                    count0++;
                }
                else if (field.isEqualValue(i, j, Field.X)){
                    countX++;
                }
                else if (field.isEqualValue(i, j, Field.EMPTY)) {
                    countFree++;
                    freePoint.y = j;
                }
            }
            if (count0 == Field.size) {
                // the gamer with weapon "0" wins
                Game.result = 2;
                return null;
            }
            else if (countX == Field.size){
                // the gamer with weapon "X" wins
                Game.result = 1;
                return null;
            }
            else if (countFree > 0 &&
                    (countX == Field.size - 1 || count0 == Field.size - 1)) {
                return freePoint;
            }
            totalCountFree+=countFree;
        }
        // column
        for (int j = 0; j < Field.size; j++) {
            freePoint = new Point(0, j);
            count0 = 0;
            countX = 0;
            countFree = 0;
            for (int i = 0; i < Field.size; i++) {
                if (field.isEqualValue(i, j, Field.ZERO)) {
                    count0++;
                }
                else if (field.isEqualValue(i, j, Field.X)){
                    countX++;
                }
                else if (field.isEqualValue(i, j, Field.EMPTY)) {
                    countFree++;
                    freePoint.x = i;
                }
            }
            if (count0 == Field.size) {
                // the gamer with weapon "0" wins
                Game.result = 2;
                return null;
            }
            else if (countX == Field.size){
                // the gamer with weapon "X" wins
                Game.result = 1;
                return null;
            }
            else if (countFree > 0 &&
                    (countX == Field.size - 1 || count0 == Field.size - 1)) {
                return freePoint;
            }
            totalCountFree+=countFree;
        }
        // the main diagonal
        count0 = 0;
        countX = 0;
        countFree = 0;
        freePoint = new Point(0, 0);
        for (int i = 0, j = 0; i < Field.size && j < Field.size; i++, j++) {
            if (field.isEqualValue(i, j, Field.ZERO)) {
                count0++;
            }
            else if (field.isEqualValue(i, j, Field.X)){
                countX++;
            }
            else if (field.isEqualValue(i, j, Field.EMPTY)) {
                countFree++;
                freePoint.x = i;
                freePoint.y = j;
            }
        }
        if (count0 == Field.size) {
            // the gamer with weapon "0" wins
            Game.result = 2;
            return null;
        }
        else if (countX == Field.size){
            // the gamer with weapon "X" wins
            Game.result = 1;
            return null;
        }
        else if (countFree > 0 &&
                (countX == Field.size - 1 || count0 == Field.size - 1)) {
            return freePoint;
        }
        totalCountFree+=countFree;

        // the secondary diagonal
        count0 = 0;
        countX = 0;
        countFree = 0;
        freePoint = new Point(0, 0);
        for (int i = Field.size - 1, j = 0; i >= 0 && j < Field.size; i--, j++) {
            if (field.isEqualValue(i, j, Field.ZERO)) {
                count0++;
            }
            else if (field.isEqualValue(i, j, Field.X)){
                countX++;
            }
            else if (field.isEqualValue(i, j, Field.EMPTY)) {
                countFree++;
                freePoint.x = i;
                freePoint.y = j;
            }
        }
        if (count0 == Field.size) {
            // the gamer with weapon "0" wins
            Game.result = 2;
            return null;
        }
        else if (countX == Field.size){
            // the gamer with weapon "X" wins
            Game.result = 1;
            return null;
        }
        else if (countFree > 0 &&
                (countX == Field.size - 1 || count0 == Field.size - 1)) {
            return freePoint;
        }
        totalCountFree+=countFree;
        if (totalCountFree == 0){
            Game.result = 0;
        }
        return null;
    }

}
