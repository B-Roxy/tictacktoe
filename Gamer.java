import java.awt.*;

/**
 * Created by Nasty on 19.04.2017.
 */
public abstract class Gamer {

    protected int weapon;
    public abstract void DoStep(Point optimalPoint, Field field);

    public void setWeapon(int weapon){
        this.weapon = weapon;
    }
}
