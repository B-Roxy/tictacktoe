import java.awt.*;
import java.util.Scanner;

/**
 * Created by Nasty on 19.04.2017.
 */
public class User extends Gamer {
    private Scanner in = new Scanner(System.in);

    public void DoStep(Point optimalPoint, Field field) {
        if (optimalPoint != null){
            System.out.format("Be careful! Check point (%d,%d) %n", optimalPoint.x + 1, optimalPoint.y + 1);
        }
        System.out.print("Input a number of string and a number of column through space: ");
        int x = in.nextInt();
        int y = in.nextInt();
        field.SetValue(x - 1, y - 1, weapon);
    }

}
